package com.joseantoniosanchez.practica5;

import android.app.Activity;
import android.os.Bundle;
import android.os.Bundle;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.*;
public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {


        switch (item.getItemId()){
            case R.id.opcionLogin:
                Dialog dialogoLogin = new Dialog(MainActivity.this);
                dialogoLogin.setContentView(R.layout.flg_login);

                Button btnautenticar = (Button) dialogoLogin.findViewById(R.id.btnautenticar);
                final EditText Cajauser = (EditText) dialogoLogin.findViewById(R.id.txtusuario);
                final EditText Cajaclave = (EditText) dialogoLogin.findViewById(R.id.txtclaveusuario);

                btnautenticar.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Toast.makeText(MainActivity.this,"Usuario: "+ Cajauser.getText().toString()+"  Clave: "+Cajaclave.getText().toString(),Toast.LENGTH_LONG).show();
                    }
                });
                dialogoLogin.show();
                break;
            case R.id.opcionRegistrar:
                Dialog dialogoRegistrar = new Dialog(MainActivity.this);
                dialogoRegistrar.setContentView(R.layout.flg_registrar);
                Button btnregistrar=(Button)dialogoRegistrar.findViewById(R.id.btnregistrar);
                final EditText nombre=(EditText)dialogoRegistrar.findViewById(R.id.txtNombres);
                final EditText apellido=(EditText)dialogoRegistrar.findViewById(R.id.txtApellidos);

                btnregistrar.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Toast.makeText(MainActivity.this, "Nombres: " + nombre.getText().toString() + "  Apellidos: " + apellido.getText().toString(), Toast.LENGTH_LONG).show();
                    }
                });
                dialogoRegistrar.show();
                break;

        }

        return true;
    }

}
